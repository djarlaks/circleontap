//
//  DrawCirclesView.m
//  CircleOnTap
//
//  Created by Maxim on 14.04.14.
//  Copyright (c) 2014 masax.ru. All rights reserved.
//

#import "DrawCirclesView.h"
@interface DrawCirclesView()
@property (nonatomic, strong) NSMutableArray *circles; // of CGPoint
@end

@implementation DrawCirclesView
@synthesize circles = _circles;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSMutableArray *)circles
{
    if (!_circles) _circles = [[NSMutableArray alloc] init];
    return _circles;
}

#define RADIUS 20
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    for (int i = 0; i < [self.circles count]; i++) {
        CGPoint point = [[self.circles objectAtIndex:i] CGPointValue];
        [path addArcWithCenter:point radius:RADIUS startAngle:0 endAngle:M_PI * 2 clockwise:YES];
        [path closePath];
    }
    
    [[UIColor blackColor] setFill];
    [path fill];
}

- (void)drawCircle:(CGPoint)point
{
    [self.circles addObject:[NSValue valueWithCGPoint:point]];
    [self setNeedsDisplay];
}

- (void)clean
{
    self.circles = nil;
    [self setNeedsDisplay];
}

@end
