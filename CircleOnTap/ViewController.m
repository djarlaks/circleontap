//
//  ViewController.m
//  CircleOnTap
//
//  Created by Maxim on 14.04.14.
//  Copyright (c) 2014 masax.ru. All rights reserved.
//

#import "ViewController.h"
#import "DrawCirclesView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapRecognized:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateRecognized) {
        [(DrawCirclesView *)self.view drawCircle:[recognizer locationInView:self.view]];
    }
}

- (IBAction)swipeRecognized:(UISwipeGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateRecognized) {
        [(DrawCirclesView *)self.view clean];
    }
}

@end
