//
//  DrawCirclesView.h
//  CircleOnTap
//
//  Created by Maxim on 14.04.14.
//  Copyright (c) 2014 masax.ru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawCirclesView : UIView

- (void)drawCircle:(CGPoint)point;
- (void)clean;

@end
